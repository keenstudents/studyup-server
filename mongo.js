const mongo = require('mongoose')

mongo.Promise = global.Promise
mongo.set('useCreateIndex', true)

// Mongo options
const mongo_options = {
  poolSize: 10,
  keepAlive: 120,
  useNewUrlParser: true
}

// MongoDB connection parameters
const mongo_db = process.env.DB_DB || 'sharetivity'
const mongo_user = process.env.DB_USER || 'db_admin'
const mongo_password = process.env.DB_PW || 'zLvFUoN0CxF9SadM'

// Dev string for local db
const mongo_string = `mongodb+srv://${mongo_user}:${mongo_password}@cluster0-udfck.mongodb.net/${mongo_db}`
const dev_mongo_string = `mongodb://127.0.0.1:27017/${mongo_db}`


// Establish connection
const connectToMongo = () => new Promise(async (resolve, reject) => {
	
	try {
		await mongo.connect(
			mongo_string,
			mongo_options
		)
		resolve();
	} catch (err) {
		reject(err);
	}

})

module.exports = {
  connectToMongo
}
