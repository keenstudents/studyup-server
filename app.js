/**
 * App
 * @summary		  App configurations for node server
 * @file 		    app.js
 * @author    	Justin Falkenstein
 * @date   	  	13.05.2018
 *
 * 
 * @copyright	(c) Copyright by Justin Falkenstein
 * 
 * 
 **/


// Dependencies
const express = require('express')
const cookieParser = require('cookie-parser')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const passport = require('passport')
const { Strategy } = require('passport-local').Strategy
const compression = require('compression')
const session = require('express-session')
const rateLimit = require('express-rate-limit')
const cors = require('cors')
const connectMongo = require('connect-mongo')
const mongoose = require('mongoose')
const MongoStore = connectMongo(session)

// For passport authentication
const UserData = require('./schemas/UserData')

// The one variable to route them all
const app = express()

// Still not sure what this is for tho
app.set('challenge', "SECRET")

// Accept requests from a proxy (our vue client)
app.enable('trust proxy')

app.use(compression())
// The console logger
app.use(morgan('dev', {
  skip: (req, res) => req.originalUrl.indexOf("sockjs-node/") > -1
}))

// Body parser to limit upload size
app.use(bodyParser.json({limit: '2mb', extended: true}))
app.use(bodyParser.urlencoded({limit: '2mb', extended: true}))
app.use(cookieParser())

// Use express session for the mongo store
app.use(
  session({
    secret: "SECRET",
    resave: true,
    cookie: {
      maxAge: 48 * 60 * 60 * 1000
    },
    maxAge: 48 * 60 * 60 * 1000,
    saveUninitialized: true,
    rolling: true,
    httpOnly: true
  })
)

// Initialize passport and express session
app.use(passport.initialize())
app.use(passport.session())

// Use mongoose local passport to authenticate users
passport.use(new Strategy(UserData.authenticate()))
passport.serializeUser(UserData.serializeUser())
passport.deserializeUser(UserData.deserializeUser())

// Don't give them credits
app.disable('x-powered-by')

// Set timeout
app.use((req, res, next) => {
	res.setTimeout(10 * 60 * 1000, () => {
		res.sendStatus(408)
	})

	next()
})

// DO NOT TOUCH
// HIGHLY SENSITIVE FUCKING PIECE OF SHIT
// CORS
app.use(cors({
  origin: function (origin, callback) {
		callback(null, true)
	},
	credentials: true

}))

// Declare API routes
const index = require('./routes/index')
const users = require('./routes/user')
const social = require('./routes/social')
const rateLimiter = new rateLimit({
	windowMs:  60 * 1000, // 1 minute
	max: 10000,
	delayMs: 0 // disabled
})

// Use API routes
app.use('/*', rateLimiter)
app.use('/', index)
app.use('/user', users)
app.use('/social', social)

module.exports = app
