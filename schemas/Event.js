const mongoose = require('mongoose')

var Event = mongoose.Schema({
	_by: {
		type: mongoose.ObjectId,
		ref: 'UserData'
	},
	title: String,
	description: String,
	date: Date,
	expires: Date,
	category: String, // TODO: eventuell stattdessen in db speichern und nach ID suchen, sonst kann jeder einfach alle Kategorien eintragen die er will.
	age_range: {
		minimum: Number,
		maximum: Number
	},
	location: [Number], // Lattitude, Longitute
	readable_address: String,
	private: Boolean, // Only invited users can join
	hidden: Boolean, // If true, it won't be listed
	limitation: Number,
	accepted_members: [{
		type: mongoose.ObjectId,
		ref: 'UserData'
	}],
	pending_members: [{
		type: mongoose.ObjectId,
		ref: 'UserData'
	}],
	banned_users: [{
		type: mongoose.ObjectId,
		ref: 'UserData'
	}],
})



module.exports = mongoose.model("Event", Event)