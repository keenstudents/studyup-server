const mongoose = require('mongoose')

var Comment = mongoose.Schema({
	text: String,
	_by: {
		type: mongoose.ObjectId,
		ref: 'UserData',
	},
	_for: {
		type: mongoose.ObjectId,
		ref: 'Event',
	}
})



module.exports = mongoose.model("Comment", Comment)