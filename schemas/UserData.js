const passportLocalMongoose = require('passport-local-mongoose')
const mongoose = require('mongoose')
const mongoosePaginated = require('mongoose-paginate')

var UserData = mongoose.Schema({
	// User
	username: String,
	avatar: String,
	bio: String,
	email: String,
  date_of_birth: Date,
	accepted_eula: Boolean, // If eula changes
	subscriptions: [{
		type: [mongoose.ObjectId],
		ref: 'Event'
	}],
	events: [{
		type: [mongoose.ObjectId],
		ref: 'Event'
	}],
	interests: [String],
	favorites: [{
		type: [mongoose.ObjectId],
		ref: 'Event'
	}],
})

UserData.index({ email: 1 })
UserData.index({ accepted_eula: 1 })

// Authentication for passport and pagination
UserData.plugin( passportLocalMongoose, { usernameField: 'email'})
UserData.plugin(mongoosePaginated)

module.exports = mongoose.model("UserData", UserData)