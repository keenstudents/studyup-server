/**
 * User functions
 * @summary		Collection of CRUD methods for user related functions
 * @file 		user.js
 * @author    	Justin Falkenstein
 * @date   	  	15.05.2018
 *
 * 
 * @copyright	(c) Copyright by Justin Falkenstein
 * 
 * 
 **/


// Dependencies
const express = require('express')
const passport = require('passport')
const router = express.Router()
const request = require('request-promise-native')


const UserData = require('../schemas/UserData')
const Event = require('../schemas/Event')

const { catchError, Response, isClientDataValid } = require('../handlers')

/**
 * LOGIN ROUTE
 * Authenticates a user through passport
 * with the mongoose local passport strategy
 * @note Passport required an explicit body with <username> and <password> field
 * 
 * @method POST Login a user
 */
router.route('/login')
	.post(async(req, res) => {		
		if (!isClientDataValid(req.body.username, req.body.password)) {
			return res.status(400).send(new Response(400, "We're missing some data from you!", null))
		}

		try {
			passport.authenticate('local', function(error, user, info) {
				if (error) return res.status(500).send(catchError(error))
				if (!user) return res.status(404).send(new Response(404, info.message, null))

				// After authentication, log the user in which creates a session and sets req.user
				req.login(user, function(error) {					
					if (error) return res.status(500).send(catchError(error))
					return res.status(200).send(new Response(200, "You are now logged in", user))
				})
			})(req, res)
	} catch (error) {
		
		return res.status(500).send(catchError(error))
	}
	
})

/**
 * LOGOUT ROUTE
 * Destroys a user's session
 * and the req user object
 * 
 * @method POST Logout this user
 */
router.route('/logout')
	.post(async(req, res) => {

		if (!req.user) return res.status(418).send(new Response(418, "You're a teapot. You can't logout if you aren't logged in", null)) 
		req.logout()
		req.session.destroy(error => {
			if (error) return res.status(500).send(catchError(error))
		})
		return res.status(200).send(new Response(200, "You've been logged out", null))
})

/**
 * REGISTER ROUTE
 * Handles the user registration
 * with passport authentication
 * 
 * @method PUT Register new user
 */
router.route('/register')
  .put(async(req, res) => {
		
		// Client data
		let { username, email, date_of_birth, password, accepted_eula } = req.body
		console.log(req.body);
		
		if (!isClientDataValid(username, email, date_of_birth, password)) {
			return res.status(400).send(new Response(400, "We're missing some data from you!", null))
		}

		// Verify captcha response
/*     try {
      captcha_verification = await request(captcha_url)
    } catch (err) {
			if (err.statusCode === 400) return res.status(400).send(new Response(400, "Invalid captcha request", null))
      else if (err.statusCode !== 400) return res.status(err.statusCode).send(new Response(err.statusCode, "Google is shit", null))
    }
		
		if (!JSON.parse(captcha_verification).success) return res.status(400).send(new Response(400, "Captcha invalid or empty", null))
		 */

		// Check if email already in use
		let user_count = await UserData.countDocuments({email}).exec()
		if (user_count > 0) return res.status(409).send(new Response(409, "There is already a user with this email!", null))

		// Create the account
		try {
			let new_user = {
				"username": username,
				"email": email,
				"date_of_birth": date_of_birth,
				"accepted_eula": accepted_eula
			}
			
			// Register user using mongoose local passport
			UserData.register(new UserData(new_user), password, (error, finalized) => {
				if (error) return res.status(500).send(catchError(error))
				return res.status(200).send(new Response(200, "Success! You may log in now", null))
			})
		} catch (error) {			
			return res.status(500).send(catchError(error))
		}
	})

/**
 * EVENT ROUTE
 * This route is for anything regarding events
 * from a certain user
 * 
 * @method GET Get my created events
 * @method PUT Create new event (public or private)
 * @method PATCH Edit event details 
 * @method DELETE Delete an event if user is owner
 */
router.route("/events")
.get(async (req, res) => {

	let user_id = req.user._id

	try {
		let parsed_user = await UserData.findOne({'_id': user_id}).select('events').exec()
		let created_events = await Event.find({'_id': { $in: parsed_user.events}}).exec() || []

		return res.status(200).send(new Response(200, null, created_events))
	} catch (error) {
		return res.status(500).send(catchError(error))
	}

})
.put(async (req, res) => {

	// Make sure to safely construct event object before creating it in the DB
	let event = {
		_by: req.user._id,
		title: req.body.title,
		description: req.body.description,
		date: req.body.date,
		expires: req.body.expires, 
		category: req.body.category,
		age_range: req.body.age_range,
		location: req.body.location,
		readable_address: req.body.readable_address,
		private: req.body.private_switch,
		hidden: req.body.hidden_switch,
		limitation: req.body.limitation
	}
	
	if (!isClientDataValid(event)) {
		return res.status(400).send(new Response(400, "We're missing some data from you!", null))
	}
	
	// Create new event and add the owner to the members
	try {
		let new_event = new Event(event)
		new_event.accepted_members.push(event._by)
		new_event.save()

		if (!new_event) return res.status(500).send(new Response(500, "Could not create event", null))

		UserData.findOneAndUpdate({'_id': event._by}, {
			$push: {
				'subscriptions': new_event._id,
				'events': new_event._id
			}
		}).exec()

		return res.status(200).send(new Response(200, "Okay! You'll be redirected to the page", null))
	} catch (error) {
		return res.status(500).send(catchError(error))
	}


})
.patch(async (req, res) => {

	// Make sure to safely construct event object before creating it in the DB
	let event = {
		title: req.body.title,
		description: req.body.description,
		date: req.body.date,
		expires: req.body.expires, 
		category: req.body.category,
		age_range: req.body.age_range,
		location: req.body.location,
		readable_address: req.body.readable_address,
		private: req.body.private_switch,
		hidden: req.body.hidden_switch,
		limitation: req.body.limitation
	}
	let user_id = req.user._id
	let event_id = req.body.event_id

	if (!isClientDataValid(event, user_id, event_id)) {
		return res.status(400).send(new Response(400, "We're missing some data from you!", null))
	}

	// Find and update the event
	try {
		let updated = await Event.findOneAndUpdate({'_id': event_id, '_by': user_id}, event).exec()
		if (!updated) return res.status(403).send(new Response(403, "Not authorized", null))
		return res.status(200).send(new Response(200, "Okay! You'll be redirected to the page", null))
	} catch (error) {
		return res.status(500).send(catchError(error))
	}


})
.delete(async (req, res) => {

	let event_id = req.body.event_id
	let user_id = req.user._id

	if (!isClientDataValid(user_id, event_id)) {
		
		return res.status(400).send(new Response(400, "We're missing some data from you!", null))
	}

	// Delete the event if the user is the owner
	try {
		let deleted = await Event.findOneAndRemove({'_id': event_id, '_by': user_id}).exec()
		
		// If not found -> either does not exist or user is not owner!
		if (!deleted) return res.status(403).send(new Response(403, "Not authorized", null))

		// Remove event from your subscriptions
		UserData.findOneAndUpdate({'_id': user_id}, {
			$pull: { 'subscriptions': event_id,  'events': event_id}
		}).exec(function(err, user) {
			if (err) throw new Error(err)
			if (!user) return res.status(404).send(new Response(400, "No user found", null))
		})
		
		return res.status(200).send(new Response(200, "Your event has been deleted!", null))
	} catch (error) {
		return res.status(500).send(catchError(error))		
	}

})

/**
 * PROFILE ROUTE
 * Handles user profile related requests
 * 
 * @method GET Get basic profile settings
 * @method PATCH Save changed settings
 * @method DELETE Deletes the own profile
 */
router.route("/profile")
.get(async (req, res) => {

	let acc_user_id = req.user._id
	let req_user_id = req.query.id

	if (!isClientDataValid(acc_user_id, req_user_id)) {
		return res.status(400).send(new Response(400, "We're missing some data from you!", null))
	}

	// Make sure not to send sensitive data, especially when
	// Getting other people's profile data
	try {
		let user = await UserData.findOne({'_id': req_user_id}).select('bio avatar username interests date_of_birth').exec()
		if (!user) return res.status(404).send(new Response(404, "Not found", null))

		return res.status(200).send(new Response(200, null, user))
		} catch (error) {
			return res.status(500).send(catchError(error))
		}
})
.patch(async (req, res) => {

	let user_id = req.user._id
	let avatar = req.body.avatar
	let bio = req.body.bio
	let interests = req.body.interests

	// All settings are optional
	let update = {}
	if (avatar) update.avatar = avatar
	if (bio) update.bio = bio
	if (interests) update.interests = interests

	try {
		UserData.findOneAndUpdate({'_id': user_id}, update).exec()
		return res.status(200).send(new Response(200, "Successfully updated your profile!", null))
	} catch (error) {
		return res.status(500).send(catchError(error))
	}
})
.delete(async (req, res) => {

	let user_id = req.user._id

	if (!isClientDataValid(user_id)) {
		return res.status(400).send(new Response(400, "We're missing some data from you!", null))
	}

	try {
		UserData.deleteOne({'_id': user_id}).exec()
		return res.status(200).send(new Response(200, "You deleted your account. We will miss you.", null))
	} catch (error) {
		return res.status(500).send(catchError(error))
	}
})

/**
 * FAVORITE ROUTE
 * Handles event favorites
 * 
 * @method GET Get favorite events (Array)
 * @method PATCH Toggle favorite
 */
router.route("/events/favorites")
	.get(async (req, res) => {

		let user_id = req.user._id
		
		if (!isClientDataValid(user_id)) {
			return res.status(400).send(new Response(400, "We're missing some data from you!", null))
		}

		// Get user's filters
		try {
			let events = await UserData.findOne({'_id': user_id}).populate('favorites').select('favorites').exec() || []
			return res.status(200).send(new Response(200, null, events.favorites))
		} catch (error) {
			return res.status(500).send(catchError(error))
		}

	})
	.patch(async (req, res) => {

		let user_id = req.user._id 
		let event_id = req.body.event_id
		let favorite = req.body.toggle || false

		if (!isClientDataValid(user_id, event_id)) {
			return res.status(400).send(new Response(400, "We're missing some data from you!", null))
		}

		let action = favorite ? { $addToSet: {'favorites': event_id}} : { $pull: {'favorites': event_id}} 

		// Find user document and push event id into favorite array
		try {
			UserData.findOneAndUpdate({'_id': user_id}, action).exec()
			return res.status(200).send(new Response(200, null, null))
		} catch (error) {
			return res.status(500).send(catchError(error))
		}
	})

module.exports = router
