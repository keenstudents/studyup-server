/**
 * Index functions
 * @summary			Route to catch all requests and also handle the index route
 * @file 				index.js
 * @author    	Justin Falkenstein
 * @date   	  	15.05.2018
 *
 * 
 * @copyright	(c) Copyright by Justin Falkenstein
 * 
 * 
 **/

const express = require('express')
const CryptoJS = require('crypto-js')
const turf = require('@turf/turf')
var router = express.Router()


const Event = require('../schemas/Event')
const { isClientDataValid, Response } = require('../handlers')

/**
 * ALL ROUTE
 * Checks and validates the request before it gets forwarded
 * 
 * @method ALL Catch all requests and validate them before execution
 */
router.all('*', function(req, res, next) {
	
	// Routes that can be accessed by guests! Be careful with them
	const unprotected_routes = [
    '/user/login',
    '/user/register'
	]

	// Decrypt if it has been encrypted
	if (req.body.enc) req.body = JSON.parse(CryptoJS.AES.decrypt(req.body.enc, 'SECRET').toString(CryptoJS.enc.Utf8))

	// Tier III client form validation (Body must not contain empty fields!)
	if (req.method !== "GET" && req.originalUrl !== '/user/logout' && !isClientDataValid(req.body))
		return res.status(400).send(new Response(400, "Server rejected client request: Incomplete data", null)) 

	// Protect all routes from guest access except for the unprotected routes above
	if (!req.user && !unprotected_routes.includes(req.originalUrl)) return res.status(403).send(new Response(403, 'Sorry, you have to be logged in to do this!', null))

	next() // -> Go on
})

/**
 * INDEX ROUTE
 * Used to retrieve events based 
 * on filters and preferences
 * 
 * @method GET Gets the most recent events (max. 100)
 */
router.route('/events')
	.get(async(req, res) => {
		console.log(req.query);
		
		// Parse query options correctly
		let categories = req.query.categories ? JSON.parse(req.query.categories) : []
		let show_open_only = (req.query.openonly === 'true') || false
		let location = req.query.loc ? JSON.parse(req.query.loc) : []
		let radius = parseInt(req.query.lto) || 5
		let page = parseInt(req.query.page) || 0 // Used for pagination
		
		if (categories.length === 0) return res.status(400).send(new Response(400,"You must at least select one category", null))

		try {

			// Set up all filters and preferences
			let filter_hidden = { 'hidden': false } // Never show hidden events on home page
			let filter_categories = { "category": { $in: categories } }
			let filter_open_only = show_open_only ? { 'private': false } : {}		
	
			// Don't get too many details here
			// No location, just the readable address
			let populare_query = { path: '_by', select: '_id username'}
			let events = await Event.find({
				$and: [
					filter_categories,
					filter_open_only,
					filter_hidden
				]
			}).select("-banned_users -accepted_members -pending_members").skip(page).limit(100).populate(populare_query).exec()
			

			if (location.length > 0) {
				let point = turf.point(location)
				let origin_circle = turf.circle(point, radius, {steps: 10, units: 'kilometers'})
				// Remove events that aren't within the radius
				events.forEach(element => {
					let compare_point = turf.point(element.location)
					let is_in_circle = turf.booleanPointInPolygon(compare_point, origin_circle)
					if (is_in_circle === false) events.splice(events.indexOf(element), 1)
				})
			}
					
			// If there aren't any events matching the criteria, send back an empty array
			if (!events || events.length < 1) events = []
			
			return res.status(200).send(new Response(200, null, events))
		} catch (error) {		
			return res.status(404).send(new Response(404, null, error))
		}

	})

module.exports = router
