/**
 * Social function
 * @summary			Collection of CRUD methods for social related functions
 * @file 				social.js
 * @author    	Justin Falkenstein
 * @date   	  	15.05.2018
 *
 * 
 * @copyright	(c) Copyright by Justin Falkenstein
 * 
 * 
 **/

const express = require('express')
let router = express.Router()
const mongoose = require('mongoose');

const UserData = require('../schemas/UserData')
const Event = require('../schemas/Event')
const Comment = require('../schemas/Comment')

const { catchError, Response, isClientDataValid } = require('../handlers')

/**
 * EVENT ROUTE
 * Handles request for one specific event
 * 
 * @note I should probably do an aggregation here,
 * but I've already spent 2 semesters ALONE with this shit,
 * so sorry, but I'm really tired of it.
 * 
 * @method GET Get specific event and its details
 */
router.route("/event")
	.get(async (req, res) => {
		
		let event_id = req.query.id
		let user_id = req.user._id

		if (!isClientDataValid(user_id, event_id)) {
			return res.status(400).send(new Response(400, "We're missing some data from you!", null))
		}

		try {			
			let event = await Event.findOne({'_id': event_id}).lean()
			if (!event) return res.status(404).send(new Response(404, "Couldn't find this event", null))
			
			// If the user is a member give the whole array, otherwise only membercount
			const membercount = event.accepted_members.length

			let isMember = false
			event.accepted_members.forEach(element => {
				if (user_id.equals(element)) isMember = true
			})
			const isOwner = event._by.equals(user_id)
			const comments = isMember? await Comment.find({'_for': event_id}).populate({path: '_by', select: 'username avatar'}) : {}

			// Manual selection
			if (!isMember) {
				delete event.accepted_members
			}
			if (!isOwner) {
				delete event.banned_users
				delete event.pending_members
			}

			// Manual populate
			let populate_path = '_by'
			if (isMember) populate_path = "_by accepted_members"
			if (isOwner) populate_path = "_by accepted_members banned_users pending_members"
			const populate_query = [{
				path: populate_path,
				select: 'username avatar'
			}]
			const populated = await Event.populate(event, populate_query)

			const result = {
				'event': populated, 
				'membercount': membercount, 
				'comments': comments
			}
			return res.status(200).send(new Response(200, null, result))

		} catch (error) {
			return res.status(500).send(catchError(error))
		}

	})
	
/**
 * JOIN ROUTE
 * Handles join requests and
 * subscribe/unsubscribe functionality
 * 
 * @method GET Gets all the subscribed and pending events
 * @method PUT Subscribe or apply to an event
 * @method DELETE Unsubscribe from an event
 */
	router.route("/event/join")
	.get(async (req, res) => {

		let user_id = req.user._id

		if (!isClientDataValid(user_id)) {
			return res.status(400).send(new Response(400, "We're missing some data from you!", null))
		}

		try {
			let populate_query = {path:'_by', select:'username'};
			let parsed_user = await UserData.findOne({'_id': user_id}).select('subscriptions').exec()
			let subscribed_events = await Event.find({'_id': { $in: parsed_user.subscriptions}}).populate(populate_query).exec();
			const pending_events = await Event.find({'pending_members': { $in: user_id}})
			return res.status(200).send(new Response(200, null, {subscribed_events, pending_events}))
		} catch (error) {
			return res.status(500).send(catchError(error))
		}
	})
	.put(async (req, res) => {

		let event_id = req.body.event_id
		let user_id = req.user._id
		if (!isClientDataValid(event_id)) {
			return res.status(400).send(new Response(400, "We're missing some data from you!", null))
		}

		// Find event to subscribe
		try {
			let event = await Event.findOne({'_id': event_id}).exec()
			if (!event) return res.status(404).send(new Response(404, "Could not find this event", null))
			if (event.accepted_members.length === event.limitation) return res.status(400).send(new Response(400, "The limit of this event has been reached. Sorry", null))
			if (event.banned_users.indexOf(user_id) !== -1) return res.status(403).send(new Response(403, "You're banned from this event", null))

			if (event.private === true) {
				if (event.pending_members.indexOf(user_id) !== -1) return res.status(409).send(new Response(409, "You are already pending! Be patient", null))
				event.pending_members.push(user_id)
				event.save()
				return res.status(200).send(new Response(200, "Applied for event. Please wait until the owner checks it out", null))
			}
			
			// Push to user and event if successfull
			if (event.accepted_members.indexOf(user_id) !== -1) return res.status(409).send(new Response(409, "You are already a member!", null))
			UserData.findOneAndUpdate({'_id': user_id}, { $push: {'subscriptions': event_id }}).exec()

			event.accepted_members.push(user_id)
			event.save()

			return res.status(200).send(new Response(200, "Successfully joined", null))
		} catch (error) {
			return res.status(500).send(catchError(error))		
		}

	})
	.delete(async (req, res) => {

		let event_id = req.body.event_id
		let user_id = req.user._id

		if (!isClientDataValid(user_id, event_id)) {
			return res.status(400).send(new Response(400, "We're missing some data from you!", null))
		}

		// Unsubscribes from an event
		try {
			Event.findOneAndUpdate({'_id': event_id}, { $pull: { 'accepted_members': user_id, 'pending_members': user_id }}).exec()
			UserData.findOneAndUpdate({'_id': user_id}, { $pull: { 'subscriptions': event_id }}).exec()

			return res.status(200).send(new Response(200, "You're out! Sad.", null))
		} catch (error) {
			return res.status(500).send(catchError(error))
		}
	})

/**
 * COMMENT ROUTE
 * Handles comments on public
 * events
 * 
 * @method ALL Checks if user is member of event
 * @method GET Get comments of an event
 * @method PUT Adds a comment to an event
 * @method DELETE Delete comment if user is poster or Owner
 */
router.route("/event/comments")
	.all(async (req, res, next) => {

		let user_id = req.user._id
		let event_id = req.query.id || req.body.event_id

		// If user wants to modify/delete a comment we can just go on
		// And check the _by field of the comment
		if (!event_id && req.body.comment_id) return next()

		// Check if member or not
		try {
			let event = await Event.findOne({'_id': event_id}).exec()
			if (!event) return res.status(404).send(new Response(404, "Event not found", null))
			if (event.accepted_members.indexOf(user_id) === -1) return res.status(403).send(new Response(403, "You aren't a member of this event!", null))
		} catch (error) {
			return res.status(500).send(catchError(error))
		}
	
		next()
	})
	.get(async (req, res) => {

		let event_id = req.query.id
		let user_id = req.user._id
		if (!isClientDataValid(event_id, user_id)) {
			return res.status(400).send(new Response(400, "We're missing some data from you!", null))
		}

		try {
			let populate_query = { path: '_by', select: '_id username'}
			let comments = await Comment.find({'_for': event_id}).populate(populate_query).exec()
			return res.status(200).send(new Response(200, null, comments))
		} catch (error) {
			return res.status(500).send(catchError(error))
		}

	})
	.put(async (req, res) => {

		let event_id = req.body.event_id
		let user_id = req.user._id
		let text = req.body.text

		if (!isClientDataValid(user_id, event_id, text)) {
			return res.status(400).send(new Response(400, "We're missing some data from you!", null))
		}

		// Make sure to push the creator in aswell
		try {
			let new_event = new Comment({
				'_by': user_id,
				'_for': event_id,
				'text': text
			})
			let event = await new_event.save()
	
			if (!event) return res.status(404).send(new Response(404, "Unknown error", null))
			return res.status(200).send(new Response(200, "Success!", {'comment_id': event._id}))
		} catch (error) {
			return res.status(500).send(catchError(error))
		}

	})
	.delete(async (req, res) => {

		let comment_id = req.body.comment_id
		let user_id = req.user._id

		if (!isClientDataValid(user_id, comment_id)) {
			return res.status(400).send(new Response(400, "We're missing some data from you!", null))
		}

		// Delete comment and check if the deleter is owner or author
		try {
			let comment = await Comment.findOne({'_id': comment_id}).exec()
			let event = await Event.findOne({'_id': comment._for}).exec()
			
			if (
				!user_id.equals(event._by) && 
				!user_id.equals(comment._by)
				) return res.status(403).send(new Response(403, "You are not allowed to do this!", null))
			
			Comment.deleteOne({'_id': comment_id}).exec()
			return res.status(200).send(new Response(200, "Removed comment!", null))
		} catch (error) {
			return res.status(500).send(catchError(error))
		}

	})

/**
 * TAKEIN ROUTE
 * Moderation route to accept 
 * incoming requests
 * 
 * @method POST Accepts a member who requested membership
 */
router.route("/event/takein")
	.post(async (req, res) => {

		let event_id = req.body.event_id
		let req_user_id = req.body.user_id
		let acc_user_id = req.user._id

		if (!isClientDataValid(acc_user_id, req_user_id, event_id)) {
			return res.status(400).send(new Response(400, "We're missing some data from you!", null))
		}

		// Accept user and remove them from the pending members list
		try {
			let event = await Event.findOneAndUpdate({ '_id': event_id, '_by': acc_user_id	}, {
				$pull: { 'pending_members': req_user_id },
				$addToSet: { 'accepted_members': req_user_id }
			})
			if (!event) return res.status(403).send(new Response(403, "Not authorized", null))

			// Update user document aswell
			await UserData.findOneAndUpdate({'_id': req_user_id}, {
				$addToSet: { 'subscriptions': event_id }
			})
		
			return res.status(200).send(new Response(200, "Done", null))
		} catch (error) {
			return res.status(500).send(catchError(error))
		}

	})

	
/**
 * TAKEOUT ROUTE
 * Moderation functionality to 
 * decline pending member requests, kick
 * existing members or bans them
 * 
 * @method POST Kicks a user no matter if they are already member or not
 */
router.route("/event/takeout")
.post(async (req, res) => {

	let event_id = req.body.event_id
	let req_user_id = req.body.user_id
	let acc_user_id = req.user._id

	if (!isClientDataValid(acc_user_id, req_user_id, event_id)) {
		return res.status(400).send(new Response(400, "We're missing some data from you!", null))
	}

	if (acc_user_id === req_user_id) return res.status(403).send(new Response(403, "You can't kick yourself. If you want to delete your event, go into the event settings.", null))

	try {
		// If the user who accepted is not allowed to do this, it'll be null
		let event = await Event.findOneAndUpdate({ '_id': event_id, '_by': acc_user_id	}, {
			$pull: {
				'pending_members': req_user_id,
				'accepted_members': req_user_id
			}
		})
		if (!event) return res.status(403).send(new Response(403, "Not authorized", null))

		
		// Update user document aswell		
		await UserData.findOneAndUpdate({'_id': req_user_id}, {
			$pull: { 'subscriptions': event_id }
		})

		return res.status(200).send(new Response(200, "Done", null))
	} catch (error) {
		return res.status(500).send(catchError(error))
	}

})

/**
 * UNBAN ROUTE
 * Used to unban banned users
 * 
 * @method PUT Bans a user for a given event
 * @method DELETE Removes a ban from a user
 */
router.route("/event/ban")
.put(async (req, res) => {
	let event_id = req.body.event_id
	let req_user_id = req.body.user_id
	let acc_user_id = req.user._id

	if (!isClientDataValid(acc_user_id, req_user_id, event_id)) {
		return res.status(400).send(new Response(400, "We're missing some data from you!", null))
	}

	try {
		let event = await Event.findOneAndUpdate({ '_id': event_id, '_by': acc_user_id }, {
			$push: {
				'banned_users': req_user_id
			}
		})
		if (!event) return res.status(403).send(new Response(403, "Not authorized", null))

		return res.status(200).send(new Response(200, "Done", null))
	} catch (error) {
		return res.status(500).send(catchError(error))
	}


})
.delete(async (req, res) => {

	let event_id = req.body.event_id
	let req_user_id = req.body.user_id
	let acc_user_id = req.user._id

	if (!isClientDataValid(acc_user_id, req_user_id, event_id)) {
		return res.status(400).send(new Response(400, "We're missing some data from you!", null))
	}

	// If the user who accepted is not allowed to do this, it'll be null
	try {
		let event = await Event.findOneAndUpdate({ '_id': event_id, '_by': acc_user_id }, {
			$pull: {
				'banned_users': req_user_id
			}
		})
		if (!event) return res.status(403).send(new Response(403, "Not authorized", null))

		return res.status(200).send(new Response(200, "Done", null))
	} catch (error) {
		return res.status(500).send(catchError(error))
	}


})
module.exports = router
