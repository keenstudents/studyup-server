/**
 * Index 
 * @summary		  Index of our node server
 * @file 		    index.js
 * @author    	Justin Falkenstein
 * @date   	  	13.05.2018
 *
 * 
 * @copyright	(c) Copyright by Justin Falkenstein
 * 
 * 
 **/


const {connectToMongo } = require('./mongo')

const init = async () => {
  
  try {
    await connectToMongo()

    var app = require('./app')
    var http = require('http')

    
    // Get port from environment and store in Express.
    var port = normalizePort(process.env.PORT || '3000')
    app.set('port', port)

    // memwatch.on('leak', info => {
    //   console.error('Memory leak detected:\n', info)
    // })

		
		// Create HTTP Server
    var server = http.createServer(app)

    // Listen on provided ports on all devices
    server.listen(port)
    server.on('error', onError)
    server.on('listening', onListening)
    
    // Normalize port into string, number or false
    function normalizePort(val) {
      var port = parseInt(val, 10)

      if (isNaN(port)) return val

      if (port >= 0) return port

      return false
    }

		// Event listener for errors
    function onError(error) {
      if (error.syscall !== 'listen') throw error

      var bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port

      // handle specific listen errors with friendly messages
      switch (error.code) {
        case 'EACCES':
          console.error(bind + ' requires elevated privileges')
          process.exit(1)
          break
        case 'EADDRINUSE':
          console.error(bind + ' is already in use')
          process.exit(1)
          break
        default:
          console.error("ERROR IN ONERROR LISTENER")
          throw error
      }
    }

    // Event listener for listening events
    function onListening() {
      var addr = server.address()
      var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port
      console.info('Listening on ' + bind)
    }
  } catch (err) {
    throw err
  }
}

init()
  .then(() => {})
  .catch(err => console.error(err))

process.on('unhandledRejection', ev => {
  console.error(11023,ev)
  //process.exit(1)
})
